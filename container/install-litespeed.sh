#!/bin/sh

set -e

mkdir /app

apk add --update \
    openssl \
    $PHP_VERSION \
    $PHP_VERSION-bcmath \
    $PHP_VERSION-bz2 \
    $PHP_VERSION-calendar \
    $PHP_VERSION-ctype \
    $PHP_VERSION-curl \
    $PHP_VERSION-dom \
    $PHP_VERSION-exif \
    $PHP_VERSION-fileinfo \
    $PHP_VERSION-ftp \
    $PHP_VERSION-gd \
    $PHP_VERSION-iconv \
    $PHP_VERSION-intl \
    $PHP_VERSION-json \
    $PHP_VERSION-litespeed \
    $PHP_VERSION-mbstring \
    $PHP_VERSION-mysqli \
    $PHP_VERSION-opcache \
    $PHP_VERSION-openssl \
    $PHP_VERSION-pdo \
    $PHP_VERSION-pdo_mysql \
    $PHP_VERSION-phar \
    $PHP_VERSION-posix \
    $PHP_VERSION-session \
    $PHP_VERSION-shmop \
    $PHP_VERSION-simplexml \
    $PHP_VERSION-soap \
    $PHP_VERSION-sockets \
    $PHP_VERSION-sysvmsg \
    $PHP_VERSION-sysvsem \
    $PHP_VERSION-sysvshm \
    $PHP_VERSION-tokenizer \
    $PHP_VERSION-xml \
    $PHP_VERSION-xmlreader \
    $PHP_VERSION-xmlwriter \
    $PHP_VERSION-zip \
    $PHP_VERSION-zlib \
    $PHP_VERSION-pecl-imagick \
    $PHP_VERSION-pecl-redis

apk add /home/packager/packages/testing/x86_64/litespeed-1.7.18-r1.apk

# symlink $PHP_VERSION to php for convenience
if [ -e /usr/bin/php ]; then
    rm /usr/bin/php
fi
ln -s /usr/bin/$PHP_VERSION /usr/bin/php

# point fcgi-bin/lsphp to the desired version
if [ -e /var/lib/litespeed/fcgi-bin/lsphp ]; then
    rm /var/lib/litespeed/fcgi-bin/lsphp
fi
ln -s /usr/bin/ls$PHP_VERSION /var/lib/litespeed/fcgi-bin/lsphp

ln -sf /dev/stdout /var/lib/litespeed/logs/access.log \
ln -sf /dev/stderr /var/lib/litespeed/logs/error.log

# listen on port 80 for regular web traffic
sed -e 's#*:8088#*:80#g' -i /etc/litespeed/httpd_config.conf

# PHP tuning
# - remove the lsphp block and put in our config
# - allow .user.ini
# - increase lsphp max to 50
# - set max requests for child to 500
sed -e '/extProcessor lsphp/,/^}$/d' \
    -i /etc/litespeed/httpd_config.conf

cat <<EOF >> /etc/litespeed/httpd_config.conf
extProcessor lsphp{
    type                            lsapi
    address                         uds://tmp/lshttpd/lsphp.sock
    maxConns                        50
    env                             PHP_LSAPI_CHILDREN=50
    env                             PHP_LSAPI_MAX_REQUESTS=500
    env                             LSPHP_ENABLE_USER_INI=on
    env                             LSAPI_AVOID_FORK=200M
    initTimeout                     60
    retryTimeout                    0
    persistConn                     1
    pcKeepAliveTimeout
    respBuffer                      0
    autoStart                       1
    path                            fcgi-bin/lsphp
    backlog                         100
    instances                       1
    priority                        0
    memSoftLimit                    0
    memHardLimit                    0
    procSoftLimit                   0
    procHardLimit                   0
}
EOF

# Remove Proc Limits for use in container
sed -e 's#\(.*procSoftLimit\s\+\)[0-9].*#\10#g' \
    -e 's#\(.*procHardLimit\s\+\)[0-9].*#\10#g' \
    -i /etc/litespeed/httpd_config.conf

# Default vhost
mkdir /var/lib/litespeed/Default
ln -s /app /var/lib/litespeed/Default/html
echo '<?php echo "DockerWest Litespeed" . PHP_EOL; phpinfo();' > /app/index.php

mkdir /var/log/litespeed/Default
ln -s /var/log/litespeed/Default /var/lib/litespeed/Default/logs
mkdir /etc/litespeed/vhosts/Default

cat <<'EOF' > /etc/litespeed/vhosts/Default/vhconf.conf
docRoot                   $VH_ROOT/html/
enableGzip                1
enableBr                  1

errorlog $VH_ROOT/logs/error.log{
  logLevel DEBUG
  rollingSize 10M
  useServer 1
}

accessLog $VH_ROOT/logs/access.log{
  logReferer 1
  logUserAgent 1
  useServer 1
}

index  {
  useServer               1
  autoIndex               0
  autoIndexURI            /_autoindex/default.php
}

expires  {
  enableExpires           1
}

accessControl  {
  allow                   *
}

rewrite  {
  enable                  1
  autoLoadHtaccess        1
  logLevel                0
}
EOF

sed -e 's#Example#Default#g' -i /etc/litespeed/httpd_config.conf

/container/user-cleanup.sh
/container/upgrade.sh
