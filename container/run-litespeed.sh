#!/bin/sh

LSWSCTRL="/var/lib/litespeed/bin/lswsctrl"

tail -f /var/log/litespeed/stderr.log 1>&2 &
tail -f /var/log/litespeed/error.log &
tail -f /var/log/litespeed/access.log &

while true; do
    if "$LSWSCTRL" status | grep '[ERROR]' > /dev/null 2>&1; then
        "$LSWSCTRL" restart
    fi

    sleep 5
done
