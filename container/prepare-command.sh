#!/bin/sh

chmod 0755 /var/log/litespeed
touch /var/log/litespeed/error.log
touch /var/log/litespeed/access.log
chmod 0744 /var/log/litespeed/error.log
mkdir -p /tmp/lshttpd

chown litespeed:litespeed -R \
    /etc/litespeed \
    /var/lib/litespeed \
    /var/log/litespeed \
    /tmp/lshttpd

chown litespeed:litespeed /app

# admin
adminpass=$(su-exec $C_USER:$C_USER \
    $PHP_VERSION /var/lib/litespeed/admin/misc/htpasswd.php \
    "DockerWest123!"
)
echo "admin:$adminpass" > /var/lib/litespeed/admin/conf/htpasswd
printf 'BE\nWVL\nBruges\nDockerWest\nDockerWest\nDockerWest\nexample@example.org\n' | su-exec $C_USER:$C_USER openssl \
    req -x509 -nodes -days 3650 \
    -newkey rsa:2048 \
    -keyout /var/lib/litespeed/admin/conf/webadmin.key \
    -out /var/lib/litespeed/admin/conf/webadmin.crt
