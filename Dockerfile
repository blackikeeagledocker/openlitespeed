FROM blackikeeagle/alpine:edge-testing as litespeedbuild

ARG PHP_VERSION=php81
ENV PHP_VERSION=${PHP_VERSION}
COPY ./package /package

RUN /container/upgrade.sh
RUN sed -e 's/^\(_php=\).*/\1php81/g' -i /package/testing/litespeed/APKBUILD
RUN apk add sudo build-base alpine-sdk
RUN adduser -D packager
RUN addgroup packager abuild
RUN echo 'packager ALL=(ALL) NOPASSWD:ALL' >/etc/sudoers.d/packager
RUN chown -R packager: /package/testing/litespeed
RUN sudo -u packager abuild-keygen -n --append --install
RUN cd /package/testing/litespeed && sudo -u packager abuild -r

FROM blackikeeagle/alpine:edge-testing

ARG PHP_VERSION=php81
ENV PHP_VERSION=${PHP_VERSION}
COPY --from=litespeedbuild /home/packager /home/packager
COPY ./container /container
RUN cp /home/packager/.abuild/*.rsa.pub /etc/apk/keys/ \
    && /container/upgrade.sh && /container/install-litespeed.sh \
    && rm /container/install-litespeed.sh \
    && rm -rf /home/packager

ENV C_USER litespeed
EXPOSE 80 7080
WORKDIR /app

CMD ["/container/run-litespeed.sh"]
